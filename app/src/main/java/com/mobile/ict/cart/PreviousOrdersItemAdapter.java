package com.mobile.ict.cart;

import android.content.Context;
import android.content.SharedPreferences;

import android.os.Bundle;
import android.preference.PreferenceManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class PreviousOrdersItemAdapter extends ArrayAdapter<SavedOrder> {

    private List<SavedOrder> orderList = new ArrayList<SavedOrder>();
    SavedOrderViewHolder viewHolder;
    HashMap<String,Double> qmap = new HashMap<String, Double>();
    Context context;
    SharedPreferences sharedPref;
    Bundle b;

    HashMap<String,Double> quantities = new HashMap<String, Double>();
    public PreviousOrdersItemAdapter(Context context, int resource) {super(context, resource);}

    public PreviousOrdersItemAdapter(Context context, int resource, List<SavedOrder> orderList) {
        super(context, resource, orderList);

        this.orderList = orderList;
        this.context = context;

    }

    static class SavedOrderViewHolder {
        TextView orderId;

        TextView timeStamp;
    }

    @Override
    public void add(SavedOrder object) {
        orderList.add(object);
        super.add(object);
    }

    @Override
    public int getCount() {
        return this.orderList.size();
    }

    @Override
    public SavedOrder getItem(int index) {
        return this.orderList.get(index);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.activity_previous_order_item, parent, false);
            viewHolder = new SavedOrderViewHolder();
            viewHolder.orderId = (TextView) row.findViewById(com.mobile.ict.cart.R.id.saved_order_orderId);
            viewHolder.timeStamp = (TextView) row.findViewById(com.mobile.ict.cart.R.id.saved_order_timeStamp);
            row.setTag(viewHolder);
        } else {
            viewHolder = (SavedOrderViewHolder)row.getTag();
        }

        sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        SavedOrder order = getItem(position);
        viewHolder.orderId.setText(order.getOrder_id() + "");

        Date time = null;
        try {
            time = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS").parse(order.getTimeStamp());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String date = new SimpleDateFormat("EEE, MMM d, yyyy").format(time);

        viewHolder.timeStamp.setText(date);

        return row;
    }


}
