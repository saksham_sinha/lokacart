package com.mobile.ict.cart;

public class DraftOrder {


    String quantityName, quantity, quantityBasePrice, quantityTotalPrice, draft_id, time_stamp;

    int position;

    public String getTime_stamp() {
        return time_stamp;
    }

    boolean box;

    public String getDraft_id() {
        return draft_id;
    }

    DraftOrder(String _quantityName, String _quantity, String _quantityBasePrice, String _quantityTotalPrice) {
        this.quantityName = _quantityName;
        this.quantity = _quantity;
        this.quantityBasePrice = _quantityBasePrice;
        this.quantityTotalPrice = _quantityTotalPrice;

    }

    DraftOrder(int _position, String _draft_id, String _timestamp, boolean _box) {

        this.position = _position;
        this.time_stamp = _timestamp;
        this.draft_id = _draft_id;
        this.box = _box;

    }


    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

}

