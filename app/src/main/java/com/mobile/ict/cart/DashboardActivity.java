package com.mobile.ict.cart;

import android.app.Activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class DashboardActivity extends Activity implements OnClickListener, OnItemSelectedListener {

    TextView newOrderButton, previousOrderButton,draftButton,orderCancelButton;
    TextView newtv,newim, prevtv,previm, canceltv,canim, drafttv,draftim;
    EditText editNumber;
    Button save,close;
    String memberNumber="",response,password,prgBarMsg,newMemberNumber;
    SharedPreferences sharedPref;
    Spinner group_spinner;
    JSONArray org;
    int orgIdAccepted[];
    String orgId[];
    String orgList[];
    String orgStatus[],orgAbbrList[];
    CharSequence[] _options ;
    boolean[] _selections;
    JSONArray orgArray;
    SharedPreferences.Editor editor;
     boolean chkStatus;
    LoginVerification loginVerification;
    AddOrgTask addOrgTask;
    boolean addOrgFlag=false,syncOrgFlag=false,selectedOrgFlag=false;
    int acceptedCount=0,rejectedCount=0;
    int dialogid=-1;
    Dialog dialog;
    private RetainedFragment dataFragment;
    SaveMobileNumberTask sm;

    @SuppressWarnings("deprecation")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        setResult(0);


        newOrderButton = (TextView) findViewById(com.mobile.ict.cart.R.id.button_new_order);
        previousOrderButton = (TextView) findViewById(com.mobile.ict.cart.R.id.button_prev_orders);
        draftButton = (TextView) findViewById(com.mobile.ict.cart.R.id.button_help);
        orderCancelButton = (TextView) findViewById(com.mobile.ict.cart.R.id.button_order_cancel);

        newtv = (TextView) findViewById(R.id.tv_new);
        newim = (TextView) findViewById(R.id.tvim_new);

        prevtv = (TextView) findViewById(R.id.tv_prev);
        previm = (TextView) findViewById(R.id.tvim_prev);


        drafttv = (TextView) findViewById(R.id.tv_draft);
        draftim = (TextView) findViewById(R.id.tvim_draft);


        canceltv = (TextView) findViewById(R.id.tv_cancel);
        canim = (TextView) findViewById(R.id.tvim_can);

        group_spinner = (Spinner) findViewById(com.mobile.ict.cart.R.id.group_spinner);
        String fontPath = "fonts/Capture_it.ttf";
        String fontPath2 = "fonts/CaviarDreams.ttf";

        Typeface tf = Typeface.createFromAsset(getAssets(), fontPath);
        Typeface tf2 = Typeface.createFromAsset(getAssets(), fontPath2);


        group_spinner.setOnItemSelectedListener(DashboardActivity.this);
        newOrderButton.setTypeface(tf);
        previousOrderButton.setTypeface(tf);
        draftButton.setTypeface(tf);
        orderCancelButton.setTypeface(tf);

        newtv.setTypeface(tf2);
        prevtv.setTypeface(tf2);
        canceltv.setTypeface(tf2);
        drafttv.setTypeface(tf2);

        newOrderButton.setOnClickListener(DashboardActivity.this);
        previousOrderButton.setOnClickListener(DashboardActivity.this);
        draftButton.setOnClickListener(DashboardActivity.this);
        orderCancelButton.setOnClickListener(DashboardActivity.this);
        newtv.setOnClickListener(DashboardActivity.this);
        newim.setOnClickListener(DashboardActivity.this);
        prevtv.setOnClickListener(DashboardActivity.this);
        previm.setOnClickListener(DashboardActivity.this);
        drafttv.setOnClickListener(DashboardActivity.this);
        draftim.setOnClickListener(DashboardActivity.this);
        canceltv.setOnClickListener(DashboardActivity.this);
        canim.setOnClickListener(DashboardActivity.this);

        sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        editor = sharedPref.edit();
        memberNumber = sharedPref.getString("mobilenumber","");

        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean("login", true);
        editor.commit();



        FragmentManager fm = getFragmentManager();
        dataFragment = (RetainedFragment) fm.findFragmentByTag("DashboardActivity");


        if (dataFragment == null) {
            dataFragment = new RetainedFragment();
            fm.beginTransaction().add(dataFragment, "DashboardActivity").commit();

            dialogid = -1;

            dataFragment.setdialogId(dialogid);

        } else {

            if (dataFragment.getdialogId() == 1) {
                dialogid=dataFragment.getdialogId();
                addOrgFlag=true;
            }


        }
        groupDetails();

        }


    @Override
    protected void onStart() {
        super.onStart();


    }


    @Override
    protected void onResume() {
        super.onResume();

    }

    @SuppressWarnings("deprecation")
    @Override
    protected void onPause() {
        super.onPause();


        if(dialogid==1)
        {
            dataFragment.setdialogId(dialogid);

        }



    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(com.mobile.ict.cart.R.menu.menu_order, menu);
        MenuItem item_home = menu.findItem(R.id.action_home);

        item_home.setVisible(false);
        item_home.setEnabled(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case com.mobile.ict.cart.R.id.action_home:
                return true;
            case com.mobile.ict.cart.R.id.action_help:
            	Intent intent = new Intent(this,HelpActivity.class);
            	startActivity(intent);
                return true;

            case R.id.action_edit_number:

                onUpdateNumber();

                 return true;



            case R.id.action_refresh:
                addOrgFlag=false;
                if(connect()){
                    JSONObject obj = new JSONObject();
                    try {
                        obj.put("phonenumber","91"+sharedPref.getString("mobilenumber",""));
                        obj.put("password",sharedPref.getString("password",""));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    prgBarMsg=getResources().getString(R.string.label_sync);
                    loginVerification = new LoginVerification(DashboardActivity.this,prgBarMsg);
                    loginVerification.execute(obj);
                }
                else{
                    Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                }

                syncOrgFlag=true;

                return true;
            case com.mobile.ict.cart.R.id.action_logout:

                chkStatus = sharedPref.getBoolean("signchk",false);


                if(!chkStatus) {


                    sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.clear();
                    editor.commit();

                }
                else
                {

                    editor.putBoolean("login",false);
                    editor.commit();

                }

                finish();
            default:
                return super.onOptionsItemSelected(item);
        }

   }







    @Override
    public void onClick(View view) {


        if(orgList.length!=0)
        {
            if(view== newOrderButton || view==newtv || view==newim){


                if(connect()){
                    Intent i = new Intent(this,NewOrderActivity.class);
                    startActivity(i);
                }
                else{

                    Toast.makeText(DashboardActivity.this,R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
                    Intent i = new Intent(this,DashboardActivity.class);
                    startActivity(i);

                }



            }
            else if(view ==  previousOrderButton || view==prevtv || view==previm){

                if(connect()){
                    Intent i = new Intent(this,PreviousOrdersActivity.class);
                    startActivity(i);
                }
                else{

                    Toast.makeText(DashboardActivity.this,R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
                    Intent i = new Intent(this,DashboardActivity.class);
                    startActivity(i);

                }

            }
            else if(view == draftButton || view==drafttv || view==draftim){
                Intent i = new Intent(this,DraftOrderActivity.class);
                startActivity(i);
            }

            else if(view == orderCancelButton || view==canceltv || view==canim){
                if(connect()){
                    Intent i = new Intent(this,CancelOrderActivity.class);
                    startActivity(i);
                }
                else{
                    Toast.makeText(DashboardActivity.this,R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_LONG).show();
                    Intent i = new Intent(this,DashboardActivity.class);
                    startActivity(i);
                }

            }


        }
        else
        {

            Bundle args = new Bundle();
            args.putInt("msg",4);

            myDialogFragment ob2 = new myDialogFragment();
            android.app.FragmentManager fm = getFragmentManager();
            ob2.setArguments(args);
            ob2.show(fm, "4");

        }



    }

    @SuppressWarnings("deprecation")
    public void groupDetails() {


        response= sharedPref.getString("json","");



        try {
            JSONObject obj = new JSONObject(response);
            orgArray = obj.getJSONArray("organizations");

            orgStatus = new String[orgArray.length()];
            acceptedCount=0;
            rejectedCount=0;
            for (int i = 0; i < orgArray.length(); i++) {
                if(orgArray.getJSONObject(i).getString("status").equals("Accepted"))
                    acceptedCount++;
                else
                if(orgArray.getJSONObject(i).getString("status").equals("Rejected"))
                    rejectedCount++;

                orgStatus[i]= orgArray.getJSONObject(i).getString("status");

            }


            orgList = new String[acceptedCount];
            orgAbbrList=new String[acceptedCount];
            orgIdAccepted=new int[acceptedCount];
            int status=0;
            for(int i=0;i<orgArray.length();i++)
            {
                if(orgStatus[i].equals("Accepted"))
                {
                     orgList[status] = orgArray.getJSONObject(i).getString("name") + "  (" + orgArray.getJSONObject(i).getString("abbr") + ")";
                     orgAbbrList[status]=orgArray.getJSONObject(i).getString("abbr");
                     orgIdAccepted[status]=orgArray.getJSONObject(i).getInt("org_id");
                     status++;
                }
            }




            if(addOrgFlag) {

                if(rejectedCount==0)
                {
                    Bundle args = new Bundle();
                    args.putInt("msg",3);

                    myDialogFragment ob2 = new myDialogFragment();
                    android.app.FragmentManager fm = getFragmentManager();
                    ob2.setArguments(args);
                    ob2.show(fm, "3");
                }
                else
                {
                    _options = new CharSequence[rejectedCount];
                    _selections = new boolean[rejectedCount];
                    orgId = new String[rejectedCount];

                    status=0;
                    for(int i=0;i<orgArray.length();i++)
                    {
                        if(orgStatus[i].equals("Rejected"))
                        {
                            _options[status]= orgArray.getJSONObject(i).getString("name") + "  (" + orgArray.getJSONObject(i).getString("abbr") + ")";
                            orgId[status]= String.valueOf(orgArray.getJSONObject(i).getInt("org_id"));
                            status++;
                        }
                    }


                    if(status!=0)
                    {

                        if(dialogid==-1)
                        showDialog( 0 );

                        if(dialogid==1)
                        {
                            group_spinner.setAdapter(new ArrayAdapter<String>(DashboardActivity.this, R.layout.spinner_row, orgList));

                            if(orgList.length!=0) {
                                group_spinner.setVisibility(View.VISIBLE);

                                group_spinner.setSelection(sharedPref.getInt("orgPos", 0));
                                editor.putString("orgIdSelected", String.valueOf(orgIdAccepted[sharedPref.getInt("orgPos", 0)]));
                                editor.commit();
                            }
                            else
                            {
                                group_spinner.setVisibility(View.INVISIBLE);

                            }
                        }
                    }

                }


            }
            else
            {
                group_spinner.setAdapter(new ArrayAdapter<String>(DashboardActivity.this, R.layout.spinner_row, orgList));

                if(orgList.length!=0) {
                    group_spinner.setVisibility(View.VISIBLE);

                    group_spinner.setSelection(sharedPref.getInt("orgPos", 0));
                    editor.putString("orgIdSelected", String.valueOf(orgIdAccepted[sharedPref.getInt("orgPos", 0)]));
                    editor.commit();
                }
                else
                {
                    group_spinner.setVisibility(View.INVISIBLE);

                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    @SuppressWarnings("deprecation")
    public void onAddOrg(View v)
    {
        addOrgFlag=true;

            if(connect()){
                JSONObject obj = new JSONObject();
                try {
                    obj.put("phonenumber","91"+sharedPref.getString("mobilenumber",""));
                    obj.put("password",sharedPref.getString("password",""));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                prgBarMsg=getResources().getString(R.string.label_please_wait);

                loginVerification = new LoginVerification(DashboardActivity.this,prgBarMsg);
                loginVerification.execute(obj);
            }
            else{
                addOrgFlag=false;
                Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

            }






    }





    public void onSyncOrg(View v)
    {
        addOrgFlag=false;
        if(connect()){
            JSONObject obj = new JSONObject();
            try {
                obj.put("phonenumber","91"+sharedPref.getString("mobilenumber",""));
                obj.put("password",sharedPref.getString("password",""));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            prgBarMsg=getResources().getString(R.string.label_sync);

            loginVerification = new LoginVerification(DashboardActivity.this,prgBarMsg);
            loginVerification.execute(obj);
        }
        else{
            Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

        }

         syncOrgFlag=true;
    }







    @SuppressWarnings("deprecation")
    @Override
    protected Dialog onCreateDialog( int id )
    {
        dialogid=1;

        return
                new AlertDialog.Builder( this )
                        .setTitle( R.string.label_alertdialog_Select_Organizations )
                        .setMultiChoiceItems(_options, _selections, new DialogSelectionClickHandler())
                        .setPositiveButton(R.string.label_alertdialog_ok, new DialogButtonClickHandler() )
                        .setNegativeButton(R.string.label_alertdialog_close, new DialogButtonClickHandler())
                        .setCancelable(false)
                        .create();
    }


    public class DialogSelectionClickHandler implements DialogInterface.OnMultiChoiceClickListener
    {
        public void onClick( DialogInterface dialog, int clicked, boolean selected )
        {
        }
    }


    public class DialogButtonClickHandler implements DialogInterface.OnClickListener
    {
        @SuppressWarnings("deprecation")
        public void onClick( DialogInterface dialog, int clicked )
        {
            dialogid=-1;
            switch( clicked )
            {
                case DialogInterface.BUTTON_POSITIVE:
                    sendOrgList();
                    addOrgFlag=false;
                    removeDialog(0);

                    break;
                case DialogInterface.BUTTON_NEGATIVE:
                    addOrgFlag=false;
                    removeDialog(0);

                    break;
            }
        }
    }

    protected void sendOrgList(){
        selectedOrgFlag=false;

        editor.putInt("orgIdSize", _options.length);
        editor.commit();

        for(int i=0;i<_options.length;i++)
        {
            editor.putInt("org_id_pos"+i,0);
            editor.commit();

        }

        for( int i = 0; i < _options.length; i++ ){
            if(_selections[i]==true)
            {

                selectedOrgFlag=true;

                editor.putString("org_id"+i,orgId[i]);
                editor.putInt("org_id_pos"+i,1);
                editor.commit();

            }

        }



        if(selectedOrgFlag)
        {

            try {
                JSONObject newOrgObj = new JSONObject();
                JSONArray orgIdArray = new JSONArray();
                JSONObject orgId;
                newOrgObj.put("phonenumber", sharedPref.getString("mobilenumber", ""));
                newOrgObj.put("email",sharedPref.getString("emailid", ""));

                int len = sharedPref.getInt("orgIdSize",0);
                for(int j=0;j<len;j++ )
                {

                    if(sharedPref.getInt("org_id_pos"+j,-1)==1)
                    {
                        orgId = new JSONObject();
                        orgId.put("org_id",sharedPref.getString("org_id"+j,"-1"));
                        orgIdArray.put(orgId);

                    }

                }
                newOrgObj.put("orglist", orgIdArray);


                if(connect()){
                    addOrgTask = new AddOrgTask(DashboardActivity.this);
                    addOrgTask.execute(newOrgObj);
                }
                else{
                    Toast.makeText(getApplicationContext(),R.string.label_toast_Please_check_internet_connection, Toast.LENGTH_SHORT).show();

                }


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        else
        {

            Toast.makeText(getApplicationContext(),R.string.label_validation_select_at_least_one_organization, Toast.LENGTH_SHORT).show();


        }


    }


    public final class myDialogFragment extends DialogFragment {

        public Dialog onCreateDialog(Bundle savedInstanceState) {
            final AlertDialog.Builder builder;
            int msg = getArguments().getInt("msg");


            switch(msg)
            {
                case 1 :    builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.label_alertdialog_Unable_to_connect_with_server__Try_again_later)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();

                                        }
                                    }
                            );
                    return builder.create();


                case 2 :    builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.label_alertdialog_Your_organisations_request_submitted_successfully)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.dismiss();

                                        }
                                    }
                            );
                    return builder.create();

                case 3 :    builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.label_alertdialog_No_organisation_available)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.dismiss();

                                        }
                                    }
                            );
                    return builder.create();

                case 4 :    builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.label_alertdialog_You_have_not_approved_by_organisation_Please_add_organisation)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.dismiss();

                                        }
                                    }
                            );
                    return builder.create();

                case 5 :    builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.label_alertdialog_Your_mobile_number_upadted_successfully)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.dismiss();

                                        }
                                    }
                            );
                    return builder.create();

                case 6 :    builder = new AlertDialog.Builder(getActivity());
                    builder.setMessage(R.string.label_alertdialog_mobile__number_exists)
                            .setPositiveButton(R.string.label_alertdialog_ok, new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {

                                            dialog.dismiss();

                                        }
                                    }
                            );
                    return builder.create();


                default:return  null;

            }



        }


    }






    public class LoginVerification extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;
        String msg;

        public LoginVerification(Context context,String msg) {
            // TODO Auto-generated constructor stub
            this.context = context;
            this.msg=msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(msg);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {
              String url1= "http://ruralict.cse.iitb.ac.in/RuralIvrs/app/login";



            JsonParser jParser = new JsonParser();
            response = jParser.getJSONFromUrl(url1,params[0],"POST",false,null,null);
            return response;

        }

        @Override
        protected void onPostExecute(String res) {

            response= res;

            pd.dismiss();

            if (response.equals("exception")) {
                addOrgFlag=false;
                Bundle args = new Bundle();
                args.putInt("msg",1);
                myDialogFragment ob2 = new myDialogFragment();
                android.app.FragmentManager fm = getFragmentManager();
                ob2.setArguments(args);
                ob2.show(fm, "1");
            } else {

                editor.putString("json", response);
                editor.commit();
                groupDetails();

            }

        }
    }



    public class  AddOrgTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;

        public AddOrgTask(Context context) {
            // TODO Auto-generated constructor stub
            this.context = context;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(getString(R.string.label_please_wait));
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            String url ="http://ruralict.cse.iitb.ac.in/RuralIvrs/app/orgsave";
            JsonParser jParser = new JsonParser();
            response = jParser.getJSONFromUrl(url,params[0],"POST",false,null,null);
            return response;
        }





        @Override
        protected void onPostExecute(String response1) {

            pd.dismiss();



            if(response1.equals("exception"))
            {
                Bundle args = new Bundle();
                args.putInt("msg",1);
                myDialogFragment ob2=new myDialogFragment();
                FragmentManager fm=getFragmentManager();
                ob2.setArguments(args);
                ob2.show(fm, "1");



            }
            else
            {


                try {
                    JSONObject resObj = new JSONObject(response1);

                    if(resObj.getString("Status").equals("Success")) {

                        Bundle args = new Bundle();
                        args.putInt("msg",2);
                        myDialogFragment ob2=new myDialogFragment();

                        FragmentManager fm=getFragmentManager();
                        ob2.setArguments(args);
                        ob2.show(fm, "2");


                    }
                    else {
                        Bundle args = new Bundle();
                        args.putInt("msg",1);


                        myDialogFragment ob2=new myDialogFragment();


                        FragmentManager fm=getFragmentManager();
                        ob2.setArguments(args);
                        ob2.show(fm, "1");



                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }





        }
    }




    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position,long id)
        {
                editor.putString("orgAbbr", orgAbbrList[position]);
                editor.putString("orgIdSelected", String.valueOf(orgIdAccepted[position]));
                editor.putInt("orgPos",position);
                editor.commit();



    }

	@Override
	public void onNothingSelected(AdapterView<?> parent)
    {
		// TODO Auto-generated method stub
		
	}





    public void onUpdateNumber()
    {
        if(connect())
        {
            dialog = new Dialog(DashboardActivity.this);
            dialog.setContentView(R.layout.edit_number);
            dialog.setTitle(R.string.label_menu_changeNumber_header);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            editNumber = (EditText) dialog.findViewById(R.id.editNumber);
            memberNumber=sharedPref.getString("mobilenumber","");
            editNumber.setText(memberNumber);
            save = (Button)dialog. findViewById(R.id.saveNumber);
            close = (Button)dialog. findViewById(R.id.close);
            save.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!editNumber.getText().toString().equals(""))
                    {
                        long i = Long.parseLong(editNumber.getText().toString());
                        long length = (long) (Math.log10(i) + 1);
                        if (length < 10 || length > 10) {
                            Toast.makeText(getApplicationContext(), R.string.label_toast_Enter_valid_mobile_number, Toast.LENGTH_LONG).show();
                        } else
                        {

                            newMemberNumber=editNumber.getText().toString();


                            JSONObject obj = new JSONObject();

                            try {
                                obj.put("phonenumber",memberNumber);
                                obj.put("phonenumber_new",newMemberNumber);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            prgBarMsg=getResources().getString(R.string.label_please_wait);
                            sm = new SaveMobileNumberTask(DashboardActivity.this,prgBarMsg);
                            sm.execute(obj);
                        }
                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), R.string.label_toast_Please_enter_mobile_number, Toast.LENGTH_LONG).show();

                    }
            }
        });



            close.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    dialog.dismiss();

                }
            });

            dialog.show();


        }
        else
        {
            Toast.makeText(this,R.string.label_toast_Please_check_WIFI_Mobile_data_connection,Toast.LENGTH_SHORT).show();
        }
    }







    public class SaveMobileNumberTask extends AsyncTask<JSONObject, String, String> {

        ProgressDialog pd;
        Context context;
        String msg,val;

        public SaveMobileNumberTask(Context context,String msg) {
            // TODO Auto-generated constructor stub
            this.context = context;
            this.msg=msg;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pd = new ProgressDialog(context);
            pd.setMessage(msg);
            pd.show();
        }

        @Override
        protected String doInBackground(JSONObject... params) {

            String serverUrl="http://ruralict.cse.iitb.ac.in/RuralIvrs/app/changenumber";
            JsonParser jParser = new JsonParser();
            String response = jParser.getJSONFromUrl(serverUrl, params[0], "POST", true, sharedPref.getString("emailid", null), sharedPref.getString("password", null));

            return response;


        }

        @Override
        protected void onPostExecute(String res) {


            Bundle args = new Bundle();

            myDialogFragment ob2 = new myDialogFragment();
            android.app.FragmentManager fm = getFragmentManager();

            pd.dismiss();

            if (res.equals("exception")) {

                args.putInt("msg",1);
                ob2.setArguments(args);
                ob2.show(fm, "1");
            } else {


                JSONObject jsonObj = null;
                try {
                    dialog.dismiss();

                    jsonObj = new JSONObject(res);


                    val = jsonObj.getString("status");

                    if (val.equals("success"))
                    {
                        editor.putString("mobilenumber",newMemberNumber);
                        editor.commit();

                        args.putInt("msg",5);
                        ob2.setArguments(args);
                        ob2.show(fm, "5");
                    }
                    else {
                        if (val.equals("new number already present.")) {
                            args.putInt("msg", 6);
                            ob2.setArguments(args);
                            ob2.show(fm, "6");
                        }
                        else
                        {
                            args.putInt("msg", 1);
                            ob2.setArguments(args);
                            ob2.show(fm, "1");
                        }
                    }
                }catch (Exception e)
                {

                }
            }

        }
    }






    public boolean connect(){
		String connection= "false";
		 ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	   	   NetworkInfo wifiNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	   	   NetworkInfo mobileNetwork = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	   	if(mobileNetwork != null && mobileNetwork.isConnected() || wifiNetwork != null && wifiNetwork.isConnected()){
	   		
	   		connection="true";
	   	}
            Boolean con = Boolean.valueOf(connection);
            return con;

	}


    @SuppressWarnings("deprecation")
    @Override
    protected void onDestroy() {

        super.onDestroy();

        if(dialogid==1) {
       removeDialog(0);
        }
    }




}
